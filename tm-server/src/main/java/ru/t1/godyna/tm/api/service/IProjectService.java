package ru.t1.godyna.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.godyna.tm.dto.model.ProjectDTO;
import ru.t1.godyna.tm.enumerated.ProjectSort;
import ru.t1.godyna.tm.enumerated.Status;

import java.util.Collection;
import java.util.List;

public interface IProjectService {

    @NotNull
    ProjectDTO changeProjectStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

    @NotNull
    ProjectDTO create(@Nullable String userId, @Nullable String name, @Nullable String description);

    @NotNull
    ProjectDTO create(@Nullable String userId, @Nullable String name);

    @Nullable
    List<ProjectDTO> findAll();

    @NotNull
    ProjectDTO removeOneById(@Nullable final String userId, @Nullable final String id);

    @NotNull
    ProjectDTO updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description);

    @Nullable
    ProjectDTO findOneById(@Nullable String userId, @Nullable String id);

    void removeAll(@NotNull String userId);

    void removeAll();

    @Nullable
    List<ProjectDTO> findAll(@Nullable String userId, @Nullable ProjectSort sort);

    long getSize(@Nullable String userId);

    @NotNull
    Collection<ProjectDTO> add(@NotNull Collection<ProjectDTO> models);

    @NotNull
    Collection<ProjectDTO> set(@NotNull Collection<ProjectDTO> models);

}
