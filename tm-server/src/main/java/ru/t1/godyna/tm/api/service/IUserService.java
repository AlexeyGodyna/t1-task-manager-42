package ru.t1.godyna.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.godyna.tm.dto.model.UserDTO;
import ru.t1.godyna.tm.enumerated.Role;

import java.util.Collection;
import java.util.List;

public interface IUserService {

    @Nullable
    UserDTO create(@Nullable String login, @Nullable String password);

    @Nullable
    UserDTO create(@Nullable String login, @Nullable String password, @Nullable String email);

    @Nullable
    UserDTO create(@Nullable String login, @Nullable String password, @Nullable Role role);

    @NotNull
    Collection<UserDTO> set(@NotNull Collection<UserDTO> users);

    @NotNull
    List<UserDTO> findAll();

    @Nullable
    UserDTO findOneById(@Nullable String id);

    @Nullable
    UserDTO findByLogin(@Nullable String login);

    @Nullable
    UserDTO findByEmail(@Nullable String email);

    @Nullable
    UserDTO removeByLogin(@Nullable String login);

    @Nullable
    UserDTO removeByEmail(@Nullable String email);

    @NotNull
    UserDTO setPassword(@Nullable String id, @Nullable String password);

    @NotNull
    UserDTO updateUser(
            @Nullable String id,
            @Nullable String firstName,
            @Nullable String lastName,
            @Nullable String middleName
    );

    boolean isLoginExist(@Nullable String login);

    boolean isMailExist(@Nullable String email);

    void lockUserByLogin(@Nullable String login);

    void unlockUserByLogin(@Nullable String login);

}
