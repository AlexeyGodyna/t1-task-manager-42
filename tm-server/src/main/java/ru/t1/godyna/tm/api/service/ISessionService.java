package ru.t1.godyna.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.godyna.tm.dto.model.SessionDTO;

import java.util.List;

public interface ISessionService {

    @NotNull
    SessionDTO add(@Nullable String userId, @Nullable SessionDTO model);

    @Nullable
    List<SessionDTO> findAll();

    @Nullable
    SessionDTO findOneById(@Nullable String userId, @Nullable String id);

    @NotNull
    SessionDTO remove(@Nullable SessionDTO model);

    @NotNull
    SessionDTO removeOneById(@Nullable String userId, @Nullable String id);

}
