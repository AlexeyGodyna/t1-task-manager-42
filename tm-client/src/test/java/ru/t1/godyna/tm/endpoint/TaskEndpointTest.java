package ru.t1.godyna.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.godyna.tm.api.endpoint.IAuthEndpoint;
import ru.t1.godyna.tm.api.endpoint.ITaskEndpoint;
import ru.t1.godyna.tm.dto.model.TaskDTO;
import ru.t1.godyna.tm.dto.request.task.*;
import ru.t1.godyna.tm.dto.request.user.UserLoginRequest;
import ru.t1.godyna.tm.dto.response.task.*;
import ru.t1.godyna.tm.dto.response.user.UserLoginResponse;
import ru.t1.godyna.tm.enumerated.Status;
import ru.t1.godyna.tm.enumerated.TaskSort;
import ru.t1.godyna.tm.marker.ISoapCategory;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;

@Category(ISoapCategory.class)
public final class TaskEndpointTest {

    @NotNull
    private final ITaskEndpoint taskEndpoint = ITaskEndpoint.newInstance();

    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance();

    @Nullable
    private String userToken;

    @NotNull
    private final Random random = new Random();

    @Before
    public void init() {
        @Nullable final UserLoginResponse loginResponse = authEndpoint.login(
                new UserLoginRequest("test", "test")
        );
        userToken = loginResponse.getToken();
    }

    @Test
    public void taskCreate() {
        @NotNull final String testTaskName = "TestTask";
        Assert.assertThrows(Exception.class, () -> taskEndpoint.createTask(
                new TaskCreateRequest())
        );
        Assert.assertThrows(Exception.class, () -> taskEndpoint.createTask(
                new TaskCreateRequest(null, null, null))
        );
        Assert.assertThrows(Exception.class, () -> taskEndpoint.createTask(
                new TaskCreateRequest(UUID.randomUUID().toString(), "taskName", "description"))
        );
        @Nullable TaskCreateResponse createResponse = taskEndpoint.createTask(
                new TaskCreateRequest(userToken, testTaskName, "description")
        );
        Assert.assertNotNull(createResponse);
        Assert.assertNotNull(createResponse.getTask());
    }

    @Test
    public void taskRemoveById() {
        Assert.assertThrows(Exception.class, () -> taskEndpoint.removeTaskById(
                new TaskRemoveByIdRequest())
        );
        Assert.assertThrows(Exception.class, () -> taskEndpoint.removeTaskById(
                new TaskRemoveByIdRequest(null, null))
        );
        Assert.assertThrows(Exception.class, () -> taskEndpoint.removeTaskById(
                new TaskRemoveByIdRequest(UUID.randomUUID().toString(), "Id"))
        );
        final int tasksLength = 10;
        @Nullable List<TaskDTO> tasks = new ArrayList<>();
        for (int i = 0; i < tasksLength; i++) {
            @NotNull final String testTaskName = "TestTask_" + i;
            @Nullable TaskCreateResponse createResponse = taskEndpoint.createTask(
                    new TaskCreateRequest(userToken, testTaskName, "description")
            );
            tasks.add(createResponse.getTask());
        }
        final int randomIndex = random.nextInt(tasksLength - 1);
        @Nullable final TaskDTO task = tasks.get(randomIndex);
        taskEndpoint.removeTaskById(new TaskRemoveByIdRequest(userToken, task.getId()));
        Assert.assertNull(taskEndpoint.showTaskById(new TaskShowByIdRequest(userToken, task.getId())).getTask());
    }

    @Test
    public void taskUpdateById() {
        @NotNull final String testTaskName = "TestTask";
        Assert.assertThrows(Exception.class, () -> taskEndpoint.updateTaskById(
                new TaskUpdateByIdRequest(null, null, null, null)
        ));
        Assert.assertThrows(Exception.class, () -> taskEndpoint.updateTaskById(
                new TaskUpdateByIdRequest(UUID.randomUUID().toString(), null, null, null)
        ));
        Assert.assertThrows(Exception.class, () -> taskEndpoint.updateTaskById(
                new TaskUpdateByIdRequest(userToken, "Id", null, null)
        ));
        @Nullable TaskCreateResponse createResponse = taskEndpoint.createTask(
                new TaskCreateRequest(userToken, testTaskName, "description")
        );
        Assert.assertNotNull(createResponse);
        Assert.assertNotNull(createResponse.getTask());
        @Nullable TaskUpdateByIdResponse updateResponse = taskEndpoint.updateTaskById(
                new TaskUpdateByIdRequest(userToken, createResponse.getTask().getId(), "testName", "testDescription")
        );
        Assert.assertNotNull(updateResponse);
        Assert.assertNotNull(updateResponse.getTask());
        Assert.assertNotEquals(createResponse.getTask().getName(), updateResponse.getTask().getName());
        Assert.assertNotEquals(createResponse.getTask().getDescription(), updateResponse.getTask().getDescription());
        Assert.assertEquals(createResponse.getTask().getId(), updateResponse.getTask().getId());
    }

    @Test
    public void taskList() {
        @NotNull final String testTaskName = "TestTask";
        Assert.assertThrows(Exception.class, () -> taskEndpoint.listTask(
                new TaskListRequest())
        );
        Assert.assertThrows(Exception.class, () -> taskEndpoint.listTask(
                new TaskListRequest(null, null))
        );
        Assert.assertThrows(Exception.class, () -> taskEndpoint.listTask(
                new TaskListRequest("", null))
        );
        Assert.assertThrows(Exception.class, () -> taskEndpoint.listTask(
                new TaskListRequest(UUID.randomUUID().toString(), null))
        );
        @Nullable TaskCreateResponse createResponse = taskEndpoint.createTask(
                new TaskCreateRequest(userToken, testTaskName, "description")
        );
        Assert.assertNotNull(createResponse);
        Assert.assertNotNull(createResponse.getTask());
        @Nullable TaskListResponse response = taskEndpoint.listTask(
                new TaskListRequest(userToken, TaskSort.BY_DEFAULT)
        );
        Assert.assertNotNull(response);
        Assert.assertNotNull(response.getTasks());
    }

    @Test
    public void taskClear() {
        Assert.assertThrows(Exception.class, () -> taskEndpoint.clearTask(
                new TaskClearRequest())
        );
        Assert.assertThrows(Exception.class, () -> taskEndpoint.clearTask(
                new TaskClearRequest(null))
        );
        Assert.assertThrows(Exception.class, () -> taskEndpoint.clearTask(
                new TaskClearRequest(UUID.randomUUID().toString()))
        );
        final int tasksLength = 10;
        for (int i = 0; i < tasksLength; i++) {
            @NotNull final String testTaskName = "TestTask_" + i;
            taskEndpoint.createTask(new TaskCreateRequest(userToken, testTaskName, "description"));
        }
        @Nullable TaskClearResponse response = taskEndpoint.clearTask(
                new TaskClearRequest(userToken)
        );
        @Nullable TaskListResponse taskList = taskEndpoint.listTask(
                new TaskListRequest(userToken, TaskSort.BY_DEFAULT)
        );
        Assert.assertNotNull(response);
    }

    @Test
    public void taskChangeStatusById() {
        @NotNull final String testTaskName = "TestTask";
        Assert.assertThrows(
                Exception.class, () -> taskEndpoint.changeTaskStatusById(
                        new TaskChangeStatusByIdRequest())
        );
        Assert.assertThrows(
                Exception.class, () -> taskEndpoint.changeTaskStatusById(
                        new TaskChangeStatusByIdRequest(null, null, null))
        );
        Assert.assertThrows(
                Exception.class, () -> taskEndpoint.changeTaskStatusById(
                        new TaskChangeStatusByIdRequest(UUID.randomUUID().toString(), null, null))
        );
        Assert.assertThrows(
                Exception.class, () -> taskEndpoint.changeTaskStatusById(
                        new TaskChangeStatusByIdRequest(userToken, null, null))
        );
        Assert.assertThrows(
                Exception.class, () -> taskEndpoint.changeTaskStatusById(
                        new TaskChangeStatusByIdRequest(userToken, testTaskName, null))
        );
        Assert.assertThrows(
                Exception.class, () -> taskEndpoint.changeTaskStatusById(
                        new TaskChangeStatusByIdRequest(userToken, UUID.randomUUID().toString(), Status.IN_PROGRESS))
        );
        @Nullable final TaskCreateResponse taskCreateResponse = taskEndpoint.createTask(
                new TaskCreateRequest(userToken, testTaskName, "description")
        );
        Assert.assertNotNull(taskCreateResponse);
        Assert.assertNotNull(taskCreateResponse.getTask());
        @Nullable final TaskChangeStatusByIdResponse response = taskEndpoint.changeTaskStatusById(
                new TaskChangeStatusByIdRequest(userToken, taskCreateResponse.getTask().getId(), Status.IN_PROGRESS)
        );
        Assert.assertNotNull(response);
        Assert.assertEquals(Status.IN_PROGRESS, response.getTask().getStatus());
    }

}
