package ru.t1.godyna.tm.command.data;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.godyna.tm.dto.request.domain.DataXmlSaveJaxBRequest;
import ru.t1.godyna.tm.enumerated.Role;

public final class DataXmlSaveJaxBCommand extends AbstractDataCommand {

    @Getter
    @NotNull
    private final String name = "data-save-xml-jaxb";

    @Getter
    @NotNull
    private final String description = "Save data in xml file.";

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA SAVE XML]");
        getDomainEndpoint().saveDataXmlJaxb(new DataXmlSaveJaxBRequest(getToken()));
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[]{ Role.ADMIN };
    }

}
